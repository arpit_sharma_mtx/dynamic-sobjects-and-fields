public with sharing class CreateSchema {

    /*
     *  MethodName: createObject
     *  Purpose: this methods calls MetaData API Methods to create Custom Object in Salesforce
    */ 
    public static void createObject(ObjectWrapper wrapper){
		MetadataService.MetadataPort service = createService();		
		MetadataService.CustomObject obj = new MetadataService.CustomObject();
		obj.fullName = wrapper.fullName;                    
		obj.label = wrapper.label;                          
		obj.pluralLabel = wrapper.pluralLabel;             
		obj.nameField = new MetadataService.CustomField();
		obj.nameField.type_x = wrapper.nameFieldType;       
		obj.nameField.label = wrapper.nameFieldLabel;       
		obj.deploymentStatus = 'Deployed';  
		obj.sharingModel = wrapper.sharingModel;      
		List<MetadataService.SaveResult> results = service.createMetadata( new MetadataService.Metadata[] { obj });		
		handleSaveResults(results[0]);
	}
	
    /*
     *  MethodName: createField
     *  Purpose: this methods calls MetaData API Methods to create Custom Field in Salesforce
    */ 
	public static void createField(FieldWrapper wrapper){
		MetadataService.MetadataPort service = createService();		
		MetadataService.CustomField customField = new MetadataService.CustomField();
		customField.fullName = wrapper.fullName;
		customField.label = wrapper.label;
		customField.type_x = wrapper.label;
		customField.length = wrapper.length;
		List<MetadataService.SaveResult> results = 		
			service.createMetadata(
				new MetadataService.Metadata[] { customField });				
		handleSaveResults(results[0]);
	}

    /*
     *  MethodName: ModifyPicklistValue
     *  Purpose: this methods helps in committing metadata inputs to salesforce
     *  Params: MetadataService.SaveResult saveResult
    */ 
    public static void handleSaveResults(MetadataService.SaveResult saveResult){
        if(saveResult==null || saveResult.success){
            return;
        }     
        // Construct error message and throw an exception
        if(saveResult.errors!=null){
            List<String> messages = new List<String>();
            messages.add((saveResult.errors.size()==1 ? 'Error ' : 'Errors ') + 'occured processing component ' + saveResult.fullName + '.');
            for(MetadataService.Error error : saveResult.errors){
                messages.add( error.message + ' (' + error.statusCode + ').' + ( error.fields!=null && error.fields.size()>0 ? ' Fields ' + String.join(error.fields, ',') + '.' : '' ) );
            }
            if(messages.size()>0){
                throw new MetadataServiceExamplesException(String.join(messages, ' '));
            }
        }
        if(!saveResult.success){
            throw new MetadataServiceExamplesException('Request failed with no specified error.');
        }      
    }

    public class ObjectWrapper{
        @AuraEnabled String fullName;
        @AuraEnabled String label;
        @AuraEnabled String pluralLabel;
        @AuraEnabled String nameFieldType;
        @AuraEnabled String nameFieldLabel;
        @AuraEnabled String sharingModel;
    }

    public class FieldWrapper{
        @AuraEnabled String fullName; // Object.fieldName
        @AuraEnabled String label;
        @AuraEnabled String fieldType;
        @AuraEnabled Integer length;
    }
}
